from django.conf.urls import patterns, include, url

from reap.views import rpp_projects, rpp_tracklist, add_project

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'rpp.views.home', name='home'),
    # url(r'^rpp/', include('rpp.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', rpp_projects),
    url(r'^tracklist', rpp_tracklist),
    url(r'^add_project', add_project),
)
