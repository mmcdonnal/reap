from django.db import models
from django.contrib import admin

# Create your models here.

class Project(models.Model):
    name = models.CharField(max_length=200)
    user = models.CharField(max_length=200)
    timestamp = models.DateTimeField()
    
    def __unicode__(self):
        return 'Project: %s' % self.name

class Track(models.Model):
    project = models.ForeignKey('Project')
    name = models.CharField(max_length=200)
    number = models.IntegerField()
    
    def __unicode__(self):
        return 'Track: %s' % self.name
    
class VST(models.Model):
    name = models.CharField(max_length=200)
    exe = models.CharField(max_length=200)
    bank = models.CharField(max_length=200)
    preset = models.CharField(max_length=200)
    notes = models.TextField()
    
    def __unicode__(self):
        return 'VST: %s' % self.name

class FXChain(models.Model):
    track = models.ForeignKey('Track')
    order = models.IntegerField()
    vst = models.ForeignKey('VST')

    def __unicode__(self):
        return 'FXChain track %s, vst %s' % (self.track.name, self.vst.name)

admin.site.register(Project)
admin.site.register(Track)
admin.site.register(VST)
admin.site.register(FXChain)

