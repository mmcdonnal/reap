import re, os
from datetime import datetime

from django.template import loader, Context
from django.shortcuts import render_to_response, redirect

from models import Project, Track, VST, FXChain


def rpp_projects(request):
    projects = Project.objects.all()
    t = loader.get_template('projects.html')
    c = Context({ 'projects': projects })
    return render_to_response('projects.html', c)

def rpp_tracklist(request):
    project_id = request.GET['project']
    project = Project.objects.get(pk=project_id)
    tracks = Track.objects.filter(project=project)
    render_tracks = []
    for track in tracks:
        render_tracks.append((track, FXChain.objects.filter(track=track)))
    t = loader.get_template('tracklist.html')
    c = Context({ 'tracks': render_tracks })
    return render_to_response('tracklist.html', c)

def parse_rpp_file(filename):
    (path, fn) = os.path.split(filename)
    (project_name, ext) = os.path.splitext(fn)
    
    class track:
        def __init__(self, track_name, number):
            self.name = track_name
            self.number = number
            self.vsts = []
            
    class vst:
        def __init__(self, vst_name, exe):
            self.name = vst_name
            self.exe = exe
            
    all_tracks = []
    
    re_name = re.compile('NAME \"(.+?)\"')
    re_vst = re.compile('<VST \"(.+?)\" \"(.+?)\"')
    re_track = re.compile('<TRACK')
    track_number = 0
    with open(filename) as f:
        get_track = False
        current_track = None
        for line in f:
            print line
            if get_track:
                get_track = False
                m = re_name.search(line)
                if m:
                    track_number += 1
                    current_track = track(track_name=m.group(1), number=track_number)
                    all_tracks.append(current_track)
            else:
                m = re_vst.search(line)
                if m:
                    current_vst = vst(vst_name = m.group(1), exe = m.group(2))
                    current_track.vsts.append(current_vst)
                else:
                    if re_track.search(line):
                        get_track = True
            
    project = Project(name=project_name, timestamp=datetime.utcnow())
    project.save()
    
    for t in all_tracks:
        track = Track(project = project, name = t.name, number = t.number)
        track.save()
        
        
        index = 0
        for v in t.vsts:
            vst = VST(name = v.name, exe = v.exe)
            vst.save()
            
            fx = FXChain(track=track, order=index, vst=vst)
            fx.save()
            index += 1
            
def add_project(request):
    rpp_file = request.GET.get('filename')
    if rpp_file and len(rpp_file):
        parse_rpp_file(rpp_file)
        return redirect('/')
    return render_to_response('add_project.html')
